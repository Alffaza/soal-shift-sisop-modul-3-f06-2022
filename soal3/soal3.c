#include <ctype.h>
#include <dirent.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

pthread_t thread[200];
char *curDir;
int count = 2;

char *getFolderName(char str[]) 
{
    char *foccur, *res;
    foccur = strchr(str, '/'); //string depan gadibaca sampe nemu /adfasdf
    if (foccur == NULL)
        return str;
    while (foccur != NULL)
    {
        res = foccur + 1;
        foccur = strchr(foccur + 1, '/');
    }
    return res;
}

char *getExtension(char str[]) //untuk dapet .txt .jpg
{
    char *foccur = getFolderName(str);
    char *res = strchr(foccur, '.');
    if (res == NULL)
        return NULL;
    else
        return (res + 1);
}

void moveFileUtil(char src[], char dest[])
{
    //src file yang mau dibaca
    //dest file yang mau dibuat (ditulis ulang)
    int ch;
    FILE *f1, *f2;
    f1 = fopen(src, "r");
    f2 = fopen(dest, "w");
    if (!f1) {
        printf("Cannot open file 1\n");
        fclose(f2);
        return;
    }
    if (!f2) {
        printf("Cannot open file 2\n");
        return;
    }
    
    while ((ch = fgetc(f1)) != EOF)
        fputc(ch, f2);

    fclose(f1);
    fclose(f2);
    
    remove(src);
    return;
}

void *moveFile(void *arg) 
{
    char *file = (char *)arg;

    char myFile[200], tempFile[200];
    strcpy(myFile, file);
    strcpy(tempFile, file);

    char *extend = getExtension(myFile); // untuk dapet extention jpg / png 
    char *fileName = getFolderName(tempFile); // untuk dapet nama file nya
    char myFolder[150];

    if (fileName[0] == '.') // dapet nama folder yang hidden
        sprintf(myFolder, "Hidden");// ganti hidden
    else if (extend == NULL)// gapunya format ganti unknown
        sprintf(myFolder, "Unknown");
    else
    {
        for (int i = 0; extend[i]; i++)
            extend[i] = tolower(extend[i]); // biar JPG jadi jpg & tar.gz dapet e tar e 
        sprintf(myFolder, "%s", extend); 
    }
    mkdir(myFolder, 0777); 

    char dest[200]; // alamat tujuan file
    sprintf(dest, "%s/%s/%s", curDir, myFolder, getFolderName(file));
    moveFileUtil(file, dest);
    return NULL;
}

void category(char *path) 
{
    char src[200];
    struct dirent **de;
    int n, ind = 0;
    n = scandir(path, &de, NULL, alphasort); //ngitung jumlah file & folder sampe habis (n = jumlah folder)
    if (n < 0)
        return;
    else 
    {
        while (ind < n) 
        {
            if (strcmp(de[ind]->d_name, ".") && strcmp(de[ind]->d_name, ".."))
            {
                strcpy(src, path);
                strcat(src, "/");
                strcat(src, de[ind]->d_name);

                if (de[ind]->d_type != DT_DIR) // folder di dalam folder tidak dibaca hanya file didalam folder
                {
                    int d;
                    d = pthread_create(&(thread[count - 2]), NULL, &moveFile, (void *)src);
                    if (d)
                        printf("File %d gagal dikategorikan\n", count - 1);
                    else
                        printf("File %d berhasil dikategorikan\n", count - 1);
                    count++;

                    for (int p = 0; p < (count - 1); p++)
                        pthread_join(thread[p], NULL);
                }  
                category(src);//rekursi sampe folder di file habis
            }
            ind++;
        }
    }
}

int main(int argc, char **argv) {
    char buf[200];
    curDir = getcwd(buf, 200);
    strcat(curDir, "/hartakarun");
    chdir(curDir);
    category(curDir); 
    return 0;
}
