#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <wait.h>

void *zip_file()
{
    pid_t child_id;
    int status;

    if (child_id == fork())
    {
        char *argv[] = {"zip", "-r", "hartakarun.zip", "-q", "/home/ubuntu/shift3/hartakarun", NULL};
        execv("/bin/zip", argv);
    }

    while (wait(&status) > 0)
        ;
}

int main(int argc, char *argv[])
{
    char *file_name = "hartakarun.zip";
    char *hostname = "127.0.0.1";
    char buffer[BUFSIZ];
    in_addr_t in_address;
    int filestream;
    int sock;
    ssize_t read_status;
    struct hostent *host;
    struct protoent *proto;
    struct sockaddr_in socket_address;
    unsigned short port = 8080;

    char cmd[100];
    scanf("%[^\n]s", cmd);
    if (strcmp(cmd, "send hartakarun.zip") != 0)        
    {
        printf("Wrong command");
        return 0;
    }

    zip_file();

    if (argc > 1)
    {
        if (strcmp(argv[1], "send") == 0)
        {
            if (argc > 2)
            {
                file_name = argv[2];
            }
            else
                exit(EXIT_FAILURE);
        }
        else
            exit(EXIT_FAILURE);
    }

    filestream = open(file_name, O_RDONLY);
    if (filestream == -1)
    {
        perror("open");
        exit(EXIT_FAILURE);
    }

    proto = getprotobyname("tcp");
    if (proto == NULL)
    {
        perror("getprotobyname");
        exit(EXIT_FAILURE);
    }

    sock = socket(AF_INET, SOCK_STREAM, proto->p_proto);
    if (sock == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    host = gethostbyname(hostname);
    if (host == NULL)
    {
        fprintf(stderr, "error: gethostbyname(\"%s\")\n", hostname);
        exit(EXIT_FAILURE);
    }

    in_address = inet_addr(inet_ntoa(*(struct in_addr *)*(host->h_addr_list)));
    if (in_address == (in_addr_t)-1)
    {
        fprintf(stderr, "error: inet_addr(\"%s\")\n", *(host->h_addr_list));
        exit(EXIT_FAILURE);
    }

    socket_address.sin_addr.s_addr = in_address;
    socket_address.sin_family = AF_INET;
    socket_address.sin_port = htons(port);

    if (connect(sock, (struct sockaddr *)&socket_address, sizeof(socket_address)) == -1)
    {
        perror("connect");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        read_status = read(filestream, buffer, BUFSIZ);
        if (read_status == 0)
            break;
        if (read_status == -1)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
        if (write(sock, buffer, read_status) == -1)
        {
            perror("write");
            exit(EXIT_FAILURE);
        }
    }
    close(filestream);
    exit(EXIT_SUCCESS);
}
