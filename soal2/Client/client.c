#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#define MAXBUFFER 1024
#define PORT 8080

struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;
char *hello = "Hello from client";
char buffer[1024] = {0};

bool strEqual(char s1[], char s2[]){
    return strcmp(s1, s2) == 0;
}

void sendMessage(char s[]){
    send(sock , s, MAXBUFFER, 0);
    if(strEqual(s,"bye"))
        exit(0);
}

void readSer(){
    valread = read(sock , buffer, MAXBUFFER);
}

void clientProcess(){
    readSer();
    if (strEqual(buffer, "RMSG")){
        char inpb[MAXBUFFER];
        char *inp = inpb;
        size_t len = MAXBUFFER;
        getline(&inp, &len, stdin);
        strcpy(inp, strtok(inp, "\n"));
        sendMessage(inp);
    }
    else if(strEqual(buffer, "SMSG")){
        readSer();
        printf("%s",buffer);
    }
    else{
    }
    strcpy(buffer, "");
}

void leaveServer(){
    send(sock , "OKBYE" , 5 , 0 );
}

int main(){
    bool isLoggedIn = false;


    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    while(true){
        clientProcess();
    }
    return 0;
}