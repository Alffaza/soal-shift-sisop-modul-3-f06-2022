#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>

#define MAXBUFFER 1024
#define PORT 8080

int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char buffer[1024] = {0};
pthread_t connectionThread;
pthread_t disconnectionThread;
pthread_t processThread;

bool greeted = false;

enum state
{
    INITIAL,
    LOGIN,
    REGISTER,
    WCOM,
    CADD,
    CSEE,
    CDOWN,
    CSUB
};
enum state currState;

typedef struct SockNode{
    int soc;
    struct SockNode *next;
} socknode;

typedef struct{
    socknode *head;
    socknode *tail;
} sockqueue;

sockqueue sq;

char currAccountName[MAXBUFFER];

bool strEqual(char s1[], char s2[]){
    // char *s3 = strtok(s1, "\n");
    // char *s4 = strtok(s2, "\n");
    return (strcmp(s1, s2) == 0);
}

void sendCommand(char s[]){
    send(new_socket, s, MAXBUFFER, 0);
}

void sendMessage(char s[]){
    sendCommand("SMSG");
    send(new_socket , s, MAXBUFFER, 0);
}
void setCurrAccount(char s[]){
    strcpy(currAccountName, s);
}

void readCli(){
    sendCommand("RMSG");
    strcpy(buffer, "");
    valread = read(new_socket , buffer, MAXBUFFER);
    printf("%s\n", buffer);
}

bool validatePass(char p[]){
    if(strlen(p) < 6){
        sendMessage("pass to short\n");
        return false;
    }
    bool hasNum, hasUpper, hasLower;
    hasNum = hasUpper = hasLower = false;
    for (int i = 0; i < strlen(p); i++){
        if(p[i] >= 'a' && p[i] <= 'z'){
            hasLower = true;
        }
        if(p[i] >= 'A' && p[i] <= 'Z'){
            hasUpper = true;
        }
        if(p[i] >= '0' && p[i] <= '9'){
            hasNum = true;
        }
    }
    if(!hasNum){
        sendMessage("needs to have a number\n");
    }
    if(!hasUpper){
        sendMessage("needs to have an uppercase letter\n");
    }
    if(!hasLower){
        sendMessage("needs to have an lowercase letter\n");
    }
    return (hasLower && hasUpper && hasNum);
}

bool usernameFound(char u[]){
    char filepath[] = "users.txt";
    FILE* fp;
    bool userFound = false;
    size_t len = MAXBUFFER;
    char * inp = NULL;
    if (!(fp = fopen(filepath, "r")))
    {
        sendMessage("user.txt file not found!\n");
    }
    while(!userFound){
        if (getline(&inp, &len, fp) == -1){
            break;
        }
        char * uname = strtok(inp, ":");
        userFound = strEqual(u, uname);
    }
    fclose(fp);
    return userFound;
}

bool doRegister(){
    char uname[MAXBUFFER], upass[MAXBUFFER];
    sendMessage("Username: ");
    readCli();
    strcpy(uname, buffer);
    if(usernameFound(uname)){
        sendMessage("username already exists!\n");
        return false;
    }
    sendMessage("Password: ");
    readCli();
    strcpy(upass, buffer);
    if(!validatePass(upass)){
        return false;
    }
    char filepath[] = "users.txt";
    FILE *fp;
    fp = fopen(filepath, "a");
    fprintf(fp, "%s:%s\n", uname, upass);
    sendMessage("user added\n");
    fclose(fp);
    return true;
}

bool doLogin(){
    char uname[MAXBUFFER], upass[MAXBUFFER];
    sendMessage("enter username: ");
    readCli();
    strcpy(uname, buffer);
    sendMessage("enter password: ");
    readCli();
    strcpy(upass, buffer);

    FILE* fp;
    bool userFound = false;
    size_t len = MAXBUFFER;
    char * inp = NULL;
    char filepath[] = "users.txt";
    char *n;
    if (!(fp = fopen(filepath, "r")))
    {
        sendMessage("user.txt file not found!\n");
    }
    while(!userFound){
        if (getline(&inp, &len, fp) == -1){
            break;
        }
        n = strtok(inp, ":");
        char * p = strtok(NULL, "\n");
        userFound = (strEqual(n, uname) && strEqual(p,upass));
    }
    sendMessage((userFound ? "log in successful\n" : "wrong username or password\n"));
    if(userFound){
        setCurrAccount(n);
    }
    return userFound;
}


void addCommand(){
    char tit[MAXBUFFER], des[MAXBUFFER], ipt[MAXBUFFER], opt[MAXBUFFER];
    strcpy(des, "../Client/");
    strcpy(ipt, "../Client/");
    strcpy(opt, "../Client/");
    sendMessage("Judul problem: ");
    readCli();
    strcpy(tit, buffer);
    sendMessage("Filepath description.txt: ");
    readCli();
    strcat(des, buffer);
    sendMessage("Filepath input.txt: ");
    readCli();
    strcat(ipt, buffer);
    sendMessage("Filepath output.txt: ");
    readCli();
    strcat(opt, buffer);
    if(!(fopen(des, "r") && fopen(ipt, "r") && fopen(opt, "r"))){
        sendMessage("invalid filepath\n");
        return;
    }
    mkdir(tit, S_IRWXU);
    
    FILE *fps, *fpt;
    char ch;

    fps = fopen(des, "r");
    strcpy(des, tit);
    strcat(des, "/description.txt");
    fpt = fopen(des, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpt);
    fclose(fps);
    fclose(fpt);

    fps = fopen(ipt, "r");
    strcpy(ipt, tit);
    strcat(ipt, "/input.txt");
    fpt = fopen(ipt, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpt);
    fclose(fps);
    fclose(fpt);

    fps = fopen(opt, "r");
    strcpy(opt, tit);
    strcat(opt, "/output.txt");
    fpt = fopen(opt, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpt);
    fclose(fps);
    fclose(fpt);

    fpt = fopen("problems.tsv", "a");
    fprintf(fpt, "%s\t%s\n", tit, currAccountName);
    fclose(fpt);
    currState = WCOM;
}

void seeCommand(){
    FILE * fp;
    fp = fopen("problems.tsv", "r");
    size_t len = MAXBUFFER;
    char * inp = NULL;
    while(getline(&inp, &len, fp) != -1){
        printf("%s\n", inp);
        char *tit = strtok(inp, "\t");
        char *aut = strtok(NULL, "\n");
        char msg[MAXBUFFER];
        sprintf(msg, "%s by %s\n", tit, aut);
        sendMessage(msg);
    }
    printf("see command executed\n");
    fclose(fp);
    currState = WCOM;
}

void downloadCommand(char s[]){
    printf("%s\n", s);
    if (opendir(s) == NULL)
    {
        sendMessage("problem does not exist!\n");
        currState = WCOM;
        return;
    }
    char destFolderPath[MAXBUFFER], srcFolderPath[MAXBUFFER], sDescPath[MAXBUFFER], sInpPath[MAXBUFFER], sOutPath[MAXBUFFER], 
        dDescPath[MAXBUFFER], dInpPath[MAXBUFFER], dOutPath[MAXBUFFER];
    sprintf(destFolderPath, "../Client/%s", s);
    sprintf(dDescPath, "%s/description.txt", destFolderPath);
    sprintf(dInpPath, "%s/input.txt", destFolderPath);
    sprintf(dOutPath, "%s/output.txt", destFolderPath);
    sprintf(srcFolderPath, "%s", s);
    sprintf(sDescPath, "%s/description.txt", srcFolderPath);
    sprintf(sInpPath, "%s/input.txt", srcFolderPath);
    sprintf(sOutPath, "%s/output.txt", srcFolderPath);
    mkdir(destFolderPath, S_IRWXU);

    FILE *fps, *fpd;
    char ch;
    
    fps = fopen(sDescPath, "r");
    fpd = fopen(dDescPath, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpd);
    fclose(fps);
    fclose(fpd);

    fps = fopen(sInpPath, "r");
    fpd = fopen(dInpPath, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpd);
    fclose(fps);
    fclose(fpd);

    fps = fopen(sOutPath, "r");
    fpd = fopen(dOutPath, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpd);
    fclose(fps);
    fclose(fpd);
    currState = WCOM;
}

void submitCommand(char tit[], char fpo[]){
    if(opendir(tit) == NULL){
        sendMessage("Problem does not exist\n");
        return;
    }
    char dOutPath[MAXBUFFER], sOutPath[MAXBUFFER];
    sprintf(dOutPath, "../Client/%s", fpo);
    sprintf(sOutPath, "%s/output.txt", tit);
    FILE *fps, *fpd;
    fpd = fopen(dOutPath, "r");
    if(fpd == NULL){
        sendMessage("Output file path not found\n");
        return;
    }
    fps = fopen(sOutPath, "r");
    char ch;
    bool hasMatched = true;
    while ((ch = fgetc(fps)) != EOF){
        if (ch != fgetc(fpd)){
            hasMatched = false;
            break;
        };
    }
    fclose(fps);
    fclose(fpd);
    sendMessage((hasMatched ? "AC\n" : "WA\n"));
}

void waitForNewClient(){
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    currState = INITIAL;
}

void *handleConnections(void *arg){
    int s = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
    socknode *sn = (socknode *)malloc(sizeof(socknode));
    sn->soc = s;
    sn->next = NULL;
    sq.head = sq.tail = sn;
    currState = INITIAL;
    new_socket = sq.head->soc;
    printf("initial connect\n");
    sendMessage("hello!\n");
    greeted = true;
    while (true)
    {
        s = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen);
        socknode *snew = (socknode *)malloc(sizeof(socknode));
        snew->soc = s;
        snew->next = NULL;
        sq.tail->next = snew;
        if(sq.head == NULL)
        {
            sq.head = snew;
            new_socket = sq.head->soc;
            sendMessage("hello!\n");
            greeted = true;
        }
        sq.tail = snew;
        printf("new connect\n");
    }
}

void *handleDisconnections(void *arg){
    while(true){
        while(sq.head == NULL);
        if(strEqual(buffer,"bye")){
            greeted = false;
            socknode *temp = sq.head;
            sq.head = sq.head->next;
            if(sq.head != NULL) {
                new_socket = sq.head->soc;
                sendMessage("hello!\n");
                greeted = true;
            }
            free(temp);
            printf("new disconnect\n");
            currState = INITIAL;
        }
    }
}

void checkCommand(char s[]){
    char *cmnd = strtok(s, " ");
    if(strEqual(cmnd, "add")){
        addCommand();
        return;
    }
    else if(strEqual(cmnd, "see")){
        seeCommand();
        return;
    }
    else if(strEqual(cmnd, "download")){
        char *a1 = strtok(NULL, " ");
        downloadCommand(a1);
        return;
    }
    else if(strEqual(cmnd, "submit")){
        char *a1 = strtok(NULL, " ");
        char *a2 = strtok(NULL, " ");
        submitCommand(a1, a2);
        return;
    }
}

void serverProcess(){
    if(sq.head == NULL)
        return;
    switch (currState)
    {
    case INITIAL:
        sendMessage("(register/login)\n");
        readCli();
        if(strEqual(buffer, "register")){
            currState = REGISTER;
        }
        else if(strEqual(buffer, "login")){
            currState = LOGIN;
        }
        else{
            //sendMessage("invalid input\n");
        }
        break;
    case LOGIN:
        if(doLogin())
            currState = WCOM;
        break;
    case REGISTER:
        if(doRegister())
            currState = INITIAL;
        break;
    case WCOM:
        sendMessage("type command: ");
        readCli();
        checkCommand(buffer);
        break;
    case CADD:
        addCommand();
        break;
    case CSEE:
        seeCommand();
        break;
    default:
        break;
    }
    strcpy(buffer, "");
}

void *handleProcess(void *arg){
    while(true){
        if(greeted)
            serverProcess();
    }
}

int main(){
    FILE * fp;
    if (!fopen("users.txt", "r"))
    {
        fopen("users.txt", "w");
    }
    if(!fopen("problems.tsv", "r")){
        fopen("problems.tsv", "w");
    }

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //waitForNewClient();
    pthread_create(&connectionThread, NULL, &handleConnections, NULL);
    pthread_create(&disconnectionThread, NULL, &handleDisconnections, NULL);
    pthread_create(&processThread, NULL, &handleProcess, NULL);
    pthread_join(connectionThread, NULL);
    pthread_join(disconnectionThread, NULL);
    pthread_join(processThread, NULL);
    exit(0);
    return 0;
}