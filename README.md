# soal-shift-sisop-modul-3-F06-2022

Ridzki Raihan Alfaza 5025201178  
Muhammad Afif Dwi Ardhiansyah 5025201212  
Antonio Taifan Montana 5025201219

---

# SOAL 1

Download dua file zip dan unzip kedua file zip secara bersamaan menggunakan thread.

Unzip pada fungsi utama:
```c
int i=0;
	int err;
	while(i<2)
	{
		err=pthread_create(&(tid[i]),NULL,&unzipThread,NULL); //membuat thread unzip
		if(err!=0) //cek error
		{
			printf("\n can't create thread UNZIP : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success UNZIP\n");
		}
		i++;
	}
	pthread_join(tid[1],NULL);
```

Fungsi thread unzip:
```c
void* unzipThread(void *arg)
{
	int status;
	char *argv1[] = {"unzip", "music.zip", "-d", "music", NULL};
	char *argv2[] = {"unzip", "quote.zip", "-d", "quote", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	
	if(pthread_equal(id,tid[0])) // thread unzip music
	{
        if (child==fork()) {
        		printf("\n unzip music\n");
		    	execv("/bin/unzip", argv1);
	    }
	else
	{
		while((wait(&status)) > 0);
	}
	}
	else if(pthread_equal(id,tid[1])) // thread unzip quote
	{
        if (child==fork()) {
        		printf("\n unzip quote\n");
		    	execv("/bin/unzip", argv2);
	    }
	else
	{
		while((wait(&status)) > 0);
	}
	
	}
	
	return NULL;
}
```

Setelah di-unzip, kedua file di-decode 64, decode ini dilakukan bersamaan untuk kedua file text dengan thread.

Fungsi utama:
```c
i=0;
	while(i<2)
	{
		err=pthread_create(&(tid[i]),NULL,&decodeThread,NULL); //membuat thread decode
		if(err!=0) //cek error
		{
			printf("\n can't create thread DECODE : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success DECODE\n");
		}
		i++;
	}
	pthread_join(tid[1],NULL);
```

Fungsi thread decode:
```c
void* decodeThread(void *arg)
{
	
	unsigned long i=0;
	pthread_t id=pthread_self();
	int status;
	
	if(pthread_equal(id,tid[0])) // thread decode music
	{
        if (child==fork()) {
        	printf("\n exe argv1\n");
		//execv("/bin/unzip", argv1);
		readTextMusic();
		
	    }
	else
	{
		while((wait(&status)) > 0);
	}
	}
	
	else if(pthread_equal(id,tid[1])) // thread decode quote
	{
        if (child==fork()) {
		printf("\n exe argv2\n");
		readTextQuote();		
	    }
	else
	{
		while((wait(&status)) > 0);
	}
	}
}
```

di dalam fungsi thread decode terdapat pemanggilan fungsi readTextQuote() dan readTextMusic(), kedua fungsi ini untuk membaca tiap file txt di dalam folder secara iteratif. Setiap membaca file text di-decode dengan fungsi base64_decode():
```c
unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}
```

Di dalam fungsi decode tersebut membutuhkan membutuhkan fungsi dan variabel berikut:
```c
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}
```

Hasil decode di atas ditulis di file text music.txt dan quote.txt. Kedua file text ini dipindah di folder hasil dengan fungsi moveFile(), sebelum itu folder hasil dibuat dengan makeFolder(), kedua fungsi tersebut sebagai berikut:
```c
void moveFile()
{
	int status;
	if (child==fork()) {
		char *argv[] = {"mv","/home/afif/music/music.txt", "/home/afif/hasil", NULL};
        	execv("/bin/mv", argv);
	}
	else
	{
		while((wait(&status)) > 0);
		int status2;
		child = fork();
		if (child==0) {
			char *argv[] = {"mv","/home/afif/quote/quote.txt", "/home/afif/hasil", NULL};
        		execv("/bin/mv", argv);
		}
		else{
			while((wait(&status)) > 0);
		}
	}
}

void makeFolder()
{
	int status;
	if (child==fork()) {
		char *argv[] = {"mkdir", "-p", "/home/afif/hasil", NULL};
        	execv("/bin/mkdir", argv);
	}
	else
	{
		while((wait(&status)) > 0);
		//moveFile();
	}
}
```

Folder hasil ini diminta untuk di-zip dengan password. Password ini adalah "mihinomenestafif".
```c
void zipFile()
{
	int status;
	child = fork();
	if (child==0) {
		char *argv[] = {"zip","-P", "mihinomenestafif", "-r", "./hasil.zip", "-q", "./hasil", NULL};
		printf("\nZip File\n");
        	execv("/bin/zip", argv);
	}
	else
	{
		while((wait(&status)) > 0);
	}
}
```

Diminta untuk menambahkan file pada zip hasil. File tersebut adalah no.txt berisi tulisan "No". Untuk melakukan ini perlu meng-unzip file hasil.zip dan membuat file text no.txt. Kedua aktivitas ini dilakukan secara bersamaan. Fungis main:
```c
i=0;
	while(i<2)
	{
		err=pthread_create(&(tid[i]),NULL,&addFileThread,NULL); //membuat thread add file
		if(err!=0) //cek error
		{
			printf("\n can't create thread Add File : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success Add File\n");
		}
		i++;
	}
	pthread_join(tid[1],NULL);
```

Fungsi addFileThread():
```c
void* addFileThread(void *arg)
{
	unsigned long i=0;
	pthread_t id=pthread_self();
	int status;
	
	if(pthread_equal(id,tid[0])) // thread unzip 2
	{
	child = fork();
        if (child==0) {
        	char *argv[] = {"unzip", "-q", "hasil.zip", NULL};
        	printf("\n unzip 2\n");
		execv("/bin/unzip", argv);
		
	    }
	else
	{
		while((wait(&status)) > 0);
	}
	}
	
	else if(pthread_equal(id,tid[1])) // thread text no
	{
	child = fork();
        if (child==0) {
		pthread_join(tid[0],NULL);
		FILE *fPtrW;
		fPtrW = fopen("no.txt", "w");
		if(fPtrW == NULL)
    		{
        		printf("Unable to create file.\n");
        		exit(EXIT_FAILURE);
    		} 
    		printf("\n create file no\n");
		fprintf(fPtrW, "No");
		fclose(fPtrW);
	    }
	else
	{
		while((wait(&status)) > 0);
	}
	}
}
```

Lalu folder hasil baru ini di-zip kembali dengan fungsi yang sama dengan sebelumnya yaitu zipFile().

---

# SOAL 2

## Client

Pada sisi client, isinya hanya function untuk menampilkan teks dan mengambil input dari terminal client, client dan server memakai port 8080 dan mempunyai ukuran buffer maksimal sebesar 1024

```c
struct sockaddr_in address;
int sock = 0, valread;
struct sockaddr_in serv_addr;
char buffer[1024] = {0};
```

variabel yang dipakai untuk komunikasi

```c
bool strEqual(char s1[], char s2[]){
    return strcmp(s1, s2) == 0;
}
```

utility function untuk membandingkan string

```c
void sendMessage(char s[]){
    send(sock , s, MAXBUFFER, 0);
    if(strEqual(s,"bye"))
        exit(0);
}
```

mengirim string ke server, jika mendapat command `bye` maka selesaikan program

```c
void readSer(){
    valread = read(sock , buffer, MAXBUFFER);
}
```

read dari server lalu memasuki nilainya ke buffer

```c
void clientProcess(){
    readSer();
    if (strEqual(buffer, "RMSG")){
        char inpb[MAXBUFFER];
        char *inp = inpb;
        size_t len = MAXBUFFER;
        getline(&inp, &len, stdin);
        strcpy(inp, strtok(inp, "\n"));
        sendMessage(inp);
    }
    else if(strEqual(buffer, "SMSG")){
        readSer();
        printf("%s",buffer);
    }
    else{
    }
    strcpy(buffer, "");
}
```

proses yang di loop pada client, jika mendapat command `RMSG` dari server maka client diminta untuk mengirim string, jika mendapat command `SMSG` client diminta untuk menampilkan string selanjutnya yang dikirim.

```c
    bool isLoggedIn = false;


    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
```

pembuatan socket dan koneksi ke server pada port 8080, jika server tidak ada print `Connection Failed`

```c
    while(true){
        clientProcess();
    }
    return 0;
```

loop function `clientProcess()`

## Server

```c
int server_fd, new_socket, valread;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char buffer[1024] = {0};
pthread_t connectionThread;
pthread_t disconnectionThread;
pthread_t processThread;

bool greeted = false;
```

variabel untuk komunikasi, thread, dan manage connection

```c
enum state
{
    INITIAL,
    LOGIN,
    REGISTER,
    WCOM,
    CADD,
    CSEE,
    CDOWN,
    CSUB
};
enum state currState;
```

enum untuk state server (note: CADD, CSEE, CDOWN, CSUB tidak jadi dipakai), WCOM merupakan singkatan dari "wait command"

```c
typedef struct SockNode{
    int soc;
    struct SockNode *next;
} socknode;

typedef struct{
    socknode *head;
    socknode *tail;
} sockqueue;

sockqueue sq;
```

queue yang berisi informasi socket.

```c
char currAccountName[MAXBUFFER];
```

nama akun yang sedang login

```c
bool strEqual(char s1[], char s2[]){
    return (strcmp(s1, s2) == 0);
}
```

utility function

```c
void sendCommand(char s[]){
    send(new_socket, s, MAXBUFFER, 0);
}

void sendMessage(char s[]){
    sendCommand("SMSG");
    send(new_socket , s, MAXBUFFER, 0);
}
```

command untuk mengirim teks ke client

```c
void setCurrAccount(char s[]){
    strcpy(currAccountName, s);
}
```

set nama akun

```c
void readCli(){
    sendCommand("RMSG");
    strcpy(buffer, "");
    valread = read(new_socket , buffer, MAXBUFFER);
    printf("%s\n", buffer);
}
```

read input dari client

```c
bool validatePass(char p[]){
    if(strlen(p) < 6){
        sendMessage("pass to short\n");
        return false;
    }
    bool hasNum, hasUpper, hasLower;
    hasNum = hasUpper = hasLower = false;
    for (int i = 0; i < strlen(p); i++){
        if(p[i] >= 'a' && p[i] <= 'z'){
            hasLower = true;
        }
        if(p[i] >= 'A' && p[i] <= 'Z'){
            hasUpper = true;
        }
        if(p[i] >= '0' && p[i] <= '9'){
            hasNum = true;
        }
    }
    if(!hasNum){
        sendMessage("needs to have a number\n");
    }
    if(!hasUpper){
        sendMessage("needs to have an uppercase letter\n");
    }
    if(!hasLower){
        sendMessage("needs to have an lowercase letter\n");
    }
    return (hasLower && hasUpper && hasNum);
}
```

cek apakah string sesuai dengan ketentuan pembuatan password

```c
bool usernameFound(char u[]){
    char filepath[] = "users.txt";
    FILE* fp;
    bool userFound = false;
    size_t len = MAXBUFFER;
    char * inp = NULL;
    if (!(fp = fopen(filepath, "r")))
    {
        sendMessage("user.txt file not found!\n");
    }
    while(!userFound){
        if (getline(&inp, &len, fp) == -1){
            break;
        }
        char * uname = strtok(inp, ":");
        userFound = strEqual(u, uname);
    }
    fclose(fp);
    return userFound;
}
```

cek apakah terdapat user `u` pada users.txt

```c
bool doRegister(){
    char uname[MAXBUFFER], upass[MAXBUFFER];
    sendMessage("Username: ");
    readCli();
    strcpy(uname, buffer);
    if(usernameFound(uname)){
        sendMessage("username already exists!\n");
        return false;
    }
    sendMessage("Password: ");
    readCli();
    strcpy(upass, buffer);
    if(!validatePass(upass)){
        return false;
    }
    char filepath[] = "users.txt";
    FILE *fp;
    fp = fopen(filepath, "a");
    fprintf(fp, "%s:%s\n", uname, upass);
    sendMessage("user added\n");
    fclose(fp);
    return true;
}
```

function untuk melakukan register

```c
bool doLogin(){
    char uname[MAXBUFFER], upass[MAXBUFFER];
    sendMessage("enter username: ");
    readCli();
    strcpy(uname, buffer);
    sendMessage("enter password: ");
    readCli();
    strcpy(upass, buffer);

    FILE* fp;
    bool userFound = false;
    size_t len = MAXBUFFER;
    char * inp = NULL;
    char filepath[] = "users.txt";
    char *n;
    if (!(fp = fopen(filepath, "r")))
    {
        sendMessage("user.txt file not found!\n");
    }
    while(!userFound){
        if (getline(&inp, &len, fp) == -1){
            break;
        }
        n = strtok(inp, ":");
        char * p = strtok(NULL, "\n");
        userFound = (strEqual(n, uname) && strEqual(p,upass));
    }
    sendMessage((userFound ? "log in successful\n" : "wrong username or password\n"));
    if(userFound){
        setCurrAccount(n);
    }
    return userFound;
}
```

function untuk melakukan login

```c
void addCommand(){
    char tit[MAXBUFFER], des[MAXBUFFER], ipt[MAXBUFFER], opt[MAXBUFFER];
    strcpy(des, "../Client/");
    strcpy(ipt, "../Client/");
    strcpy(opt, "../Client/");
    sendMessage("Judul problem: ");
    readCli();
    strcpy(tit, buffer);
    sendMessage("Filepath description.txt: ");
    readCli();
    strcat(des, buffer);
    sendMessage("Filepath input.txt: ");
    readCli();
    strcat(ipt, buffer);
    sendMessage("Filepath output.txt: ");
    readCli();
    strcat(opt, buffer);
    if(!(fopen(des, "r") && fopen(ipt, "r") && fopen(opt, "r"))){
        sendMessage("invalid filepath\n");
        return;
    }
    mkdir(tit, S_IRWXU);

    FILE *fps, *fpt;
    char ch;

    fps = fopen(des, "r");
    strcpy(des, tit);
    strcat(des, "/description.txt");
    fpt = fopen(des, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpt);
    fclose(fps);
    fclose(fpt);

    fps = fopen(ipt, "r");
    strcpy(ipt, tit);
    strcat(ipt, "/input.txt");
    fpt = fopen(ipt, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpt);
    fclose(fps);
    fclose(fpt);

    fps = fopen(opt, "r");
    strcpy(opt, tit);
    strcat(opt, "/output.txt");
    fpt = fopen(opt, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpt);
    fclose(fps);
    fclose(fpt);

    fpt = fopen("problems.tsv", "a");
    fprintf(fpt, "%s\t%s\n", tit, currAccountName);
    fclose(fpt);
    currState = WCOM;
}
```

function command `add` untuk copy file dari client ke server

```c
void seeCommand(){
    FILE * fp;
    fp = fopen("problems.tsv", "r");
    size_t len = MAXBUFFER;
    char * inp = NULL;
    while(getline(&inp, &len, fp) != -1){
        printf("%s\n", inp);
        char *tit = strtok(inp, "\t");
        char *aut = strtok(NULL, "\n");
        char msg[MAXBUFFER];
        sprintf(msg, "%s by %s\n", tit, aut);
        sendMessage(msg);
    }
    printf("see command executed\n");
    fclose(fp);
    currState = WCOM;
}
```

function command `see` untuk menampilkan isi dari problems.tsv

```c
void downloadCommand(char s[]){
    printf("%s\n", s);
    if (opendir(s) == NULL)
    {
        sendMessage("problem does not exist!\n");
        currState = WCOM;
        return;
    }
    char destFolderPath[MAXBUFFER], srcFolderPath[MAXBUFFER], sDescPath[MAXBUFFER], sInpPath[MAXBUFFER], sOutPath[MAXBUFFER],
        dDescPath[MAXBUFFER], dInpPath[MAXBUFFER], dOutPath[MAXBUFFER];
    sprintf(destFolderPath, "../Client/%s", s);
    sprintf(dDescPath, "%s/description.txt", destFolderPath);
    sprintf(dInpPath, "%s/input.txt", destFolderPath);
    sprintf(dOutPath, "%s/output.txt", destFolderPath);
    sprintf(srcFolderPath, "%s", s);
    sprintf(sDescPath, "%s/description.txt", srcFolderPath);
    sprintf(sInpPath, "%s/input.txt", srcFolderPath);
    sprintf(sOutPath, "%s/output.txt", srcFolderPath);
    mkdir(destFolderPath, S_IRWXU);

    FILE *fps, *fpd;
    char ch;

    fps = fopen(sDescPath, "r");
    fpd = fopen(dDescPath, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpd);
    fclose(fps);
    fclose(fpd);

    fps = fopen(sInpPath, "r");
    fpd = fopen(dInpPath, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpd);
    fclose(fps);
    fclose(fpd);

    fps = fopen(sOutPath, "r");
    fpd = fopen(dOutPath, "w");
    while ((ch = fgetc(fps)) != EOF) fputc(ch, fpd);
    fclose(fps);
    fclose(fpd);
    currState = WCOM;
}
```

function untuk command `download` untuk copy file dari server ke client

```c
void submitCommand(char tit[], char fpo[]){
    if(opendir(tit) == NULL){
        sendMessage("Problem does not exist\n");
        return;
    }
    char dOutPath[MAXBUFFER], sOutPath[MAXBUFFER];
    sprintf(dOutPath, "../Client/%s", fpo);
    sprintf(sOutPath, "%s/output.txt", tit);
    FILE *fps, *fpd;
    fpd = fopen(dOutPath, "r");
    if(fpd == NULL){
        sendMessage("Output file path not found\n");
        return;
    }
    fps = fopen(sOutPath, "r");
    char ch;
    bool hasMatched = true;
    while ((ch = fgetc(fps)) != EOF){
        if (ch != fgetc(fpd)){
            hasMatched = false;
            break;
        };
    }
    fclose(fps);
    fclose(fpd);
    sendMessage((hasMatched ? "AC\n" : "WA\n"));
}
```

function untuk command `submit` untuk membandingkan file input dan output

```c
void *handleConnections(void *arg){
    int s = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
    socknode *sn = (socknode *)malloc(sizeof(socknode));
    sn->soc = s;
    sn->next = NULL;
    sq.head = sq.tail = sn;
    currState = INITIAL;
    new_socket = sq.head->soc;
    printf("initial connect\n");
    sendMessage("hello!\n");
    greeted = true;
    while (true)
    {
        s = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen);
        socknode *snew = (socknode *)malloc(sizeof(socknode));
        snew->soc = s;
        snew->next = NULL;
        sq.tail->next = snew;
        if(sq.head == NULL)
        {
            sq.head = snew;
            new_socket = sq.head->soc;
            sendMessage("hello!\n");
            greeted = true;
        }
        sq.tail = snew;
        printf("new connect\n");
    }
}
```

thread untuk server selalu dapat mengambil koneksi baru

```c
void *handleDisconnections(void *arg){
    while(true){
        while(sq.head == NULL);
        if(strEqual(buffer,"bye")){
            greeted = false;
            socknode *temp = sq.head;
            sq.head = sq.head->next;
            if(sq.head != NULL) {
                new_socket = sq.head->soc;
                sendMessage("hello!\n");
                greeted = true;
            }
            free(temp);
            printf("new disconnect\n");
            currState = INITIAL;
        }
    }
}
```

thread untuk cek apabila ada disconnection baru lalu memindahkan active client ke client selanjutnya pada queue

```c
void checkCommand(char s[]){
    char *cmnd = strtok(s, " ");
    if(strEqual(cmnd, "add")){
        addCommand();
        return;
    }
    else if(strEqual(cmnd, "see")){
        seeCommand();
        return;
    }
    else if(strEqual(cmnd, "download")){
        char *a1 = strtok(NULL, " ");
        downloadCommand(a1);
        return;
    }
    else if(strEqual(cmnd, "submit")){
        char *a1 = strtok(NULL, " ");
        char *a2 = strtok(NULL, " ");
        submitCommand(a1, a2);
        return;
    }
}
```

function untuk cek command apa yang dikirim dan run function sesuai dengan argumen yang dikasih

```c
void serverProcess(){
    if(sq.head == NULL)
        return;
    switch (currState)
    {
    case INITIAL:
        sendMessage("(register/login)\n");
        readCli();
        if(strEqual(buffer, "register")){
            currState = REGISTER;
        }
        else if(strEqual(buffer, "login")){
            currState = LOGIN;
        }
        else{
            //sendMessage("invalid input\n");
        }
        break;
    case LOGIN:
        if(doLogin())
            currState = WCOM;
        break;
    case REGISTER:
        if(doRegister())
            currState = INITIAL;
        break;
    case WCOM:
        sendMessage("type command: ");
        readCli();
        checkCommand(buffer);
        break;
    case CADD:
        addCommand();
        break;
    case CSEE:
        seeCommand();
        break;
    default:
        break;
    }
    strcpy(buffer, "");
}
```

function server yang akan selalu diloop

```c
void *handleProcess(void *arg){
    while(true){
        if(greeted)
            serverProcess();
    }
}
```

loop `serverProcess()` jika server sudah kirim message "hello"

```c
int main(){
    FILE * fp;
    if (!fopen("users.txt", "r"))
    {
        fopen("users.txt", "w");
    }
    if(!fopen("problems.tsv", "r")){
        fopen("problems.tsv", "w");
    }

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //waitForNewClient();
    pthread_create(&connectionThread, NULL, &handleConnections, NULL);
    pthread_create(&disconnectionThread, NULL, &handleDisconnections, NULL);
    pthread_create(&processThread, NULL, &handleProcess, NULL);
    pthread_join(connectionThread, NULL);
    pthread_join(disconnectionThread, NULL);
    pthread_join(processThread, NULL);
    exit(0);
    return 0;
}
```

main function yang berisi pembuatan file, pembuatan socket, dan pembuatan thread.

## Screenshot

<img src="soal2/img/1.png">
<img src="soal2/img/2.png">
<img src="soal2/img/3.png">
<img src="soal2/img/4.png">
<img src="soal2/img/5.png">
<img src="soal2/img/6.png">
<img src="soal2/img/7.png">
<img src="soal2/img/8.png">

# SOAL 3

Pada soal 3,Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

## Mengextract Zip

Mengextract zip hartakarun menggunakan cara manual di terminal yaitu

```
    unzip hartakarun.zip -d ./shift3/
```

## Menata file pada folder hartakarun

Menata file hartakarun dengan menggunakan program rekursif

```c
    int main(int argc, char **argv) {
        char buf[200];
        curDir = getcwd(buf, 200);
        strcat(curDir, "/hartakarun");
        chdir(curDir);
        category(curDir);
        return 0;
}
```

Dengan fungsi kategori secara rekursif sebagai berikut

```c
void category(char *path)
{
    char src[200];
    struct dirent **de;
    int n, ind = 0;
    n = scandir(path, &de, NULL, alphasort); //ngitung jumlah file & folder sampe habis (n = jumlah folder)
    if (n < 0)
        return;
    else
    {
        while (ind < n)
        {
            if (strcmp(de[ind]->d_name, ".") && strcmp(de[ind]->d_name, ".."))
            {
                strcpy(src, path);
                strcat(src, "/");
                strcat(src, de[ind]->d_name);

                if (de[ind]->d_type != DT_DIR) // folder di dalam folder tidak dibaca hanya file didalam folder
                {
                    int d;
                    d = pthread_create(&(thread[count - 2]), NULL, &moveFile, (void *)src);
                    if (d)
                        printf("File %d gagal dikategorikan\n", count - 1);
                    else
                        printf("File %d berhasil dikategorikan\n", count - 1);
                    count++;

                    for (int p = 0; p < (count - 1); p++)
                        pthread_join(thread[p], NULL);
                }
                category(src);//rekursi sampe folder di file habis
            }
            ind++;
        }
    }
}
```

1. Membuat variabel `folder` dan `de`, kemudian membuka folder hartakarun sesuai path pada variabel `folder` dan membaca isi semua folder hartakarun dimana setiap file yang ada pada folder tersebut akan diassign di variabel `de` dengan mengabaikan file `.` dan `..`.
2. Scandir untuk menghitung jumlah file pada folder harta karun.
3. Menggunakan perulangan while dan category(src) agar program berjalan sacara rekursif
4. pthread_create untuk membuat thread setiap file yang akan dipindah
5. Menggunakan fungsi moveFile untuk memindahkan file
6. Menggunakan pthread_join untuk menjalankan semua thread secara bersamaan

```c
void *moveFile(void *arg)
{
    char *file = (char *)arg;

    char myFile[200], tempFile[200];
    strcpy(myFile, file);
    strcpy(tempFile, file);

    char *extend = getExtension(myFile); // untuk dapet extention jpg / png
    char *fileName = getFolderName(tempFile); // untuk dapet nama file nya
    char myFolder[150];

    if (fileName[0] == '.') // dapet nama folder yang hidden
        sprintf(myFolder, "Hidden");// ganti hidden
    else if (extend == NULL)// gapunya format ganti unknown
        sprintf(myFolder, "Unknown");
    else
    {
        for (int i = 0; extend[i]; i++)
            extend[i] = tolower(extend[i]); // biar JPG jadi jpg & tar.gz dapet e tar e
        sprintf(myFolder, "%s", extend);
    }
    mkdir(myFolder, 0777);

    char dest[200]; // alamat tujuan file
    sprintf(dest, "%s/%s/%s", curDir, myFolder, getFolderName(file));
    moveFileUtil(file, dest);
    return NULL;
}
```

1. Fungsi berikut untuk memindahkan file
2. Didalam fungsi moveFile memanggil fungsi getExtension untuk mendapat kan extension file seperti JPG, jpg, png dan merubahnya menjadi huruf kecil dengan menggunakan tolower
3. Dan memanggil fungsi getFolderName untuk mendapatkan nama file yang akan dipindah
4. Jika file tersebut bertipe hidden maka akan dimasukkan ke folder Hidden dan untuk file yang tidak memiliki format maka akan dimasukkan ke folder Unknown
5. moveFileUtil untuk memindahkan file sesuai dan menulis ulang semua file dengan lokasi yang dituju

```c
void moveFileUtil(char src[], char dest[])
{
    //src file yang mau dibaca
    //dest file yang mau dibuat (ditulis ulang)
    int ch;
    FILE *f1, *f2;
    f1 = fopen(src, "r");
    f2 = fopen(dest, "w");
    if (!f1) {
        printf("Cannot open file 1\n");
        fclose(f2);
        return;
    }
    if (!f2) {
        printf("Cannot open file 2\n");
        return;
    }

    while ((ch = fgetc(f1)) != EOF)
        fputc(ch, f2);

    fclose(f1);
    fclose(f2);

    remove(src);
    return;
}
```

1. Fungsi menulis ulang file dan ditempatkan pada file yang dituju
2. Menggunakan variable src sebagai file yang akan ditulis ulang dan dest sebagai file yang ditulis ulang
3. Setelah file src ditulis ulang maka file tersebut akan dihapus

berikut fungsi untuk mendapatkan nama file dan extension file

```c
char *getFolderName(char str[])
{
    char *foccur, *res;
    foccur = strchr(str, '/'); //string depan gadibaca sampe nemu /adfasdf
    if (foccur == NULL)
        return str;
    while (foccur != NULL)
    {
        res = foccur + 1;
        foccur = strchr(foccur + 1, '/');
    }
    return res;
}

char *getExtension(char str[]) //untuk dapet .txt .jpg
{
    char *foccur = getFolderName(str);
    char *res = strchr(foccur, '.');
    if (res == NULL)
        return NULL;
    else
        return (res + 1);
}
```

```c
int main()
{
    char *file_name = "hartakarun.zip";
    char buffer[BUFSIZ];
    int client_fd;
    int enable_reuseaddr = 1;
    int filestream;
    int server_fd;
    socklen_t client_len;
    ssize_t read_status;
    struct protoent *proto;
    struct sockaddr_in client_address, server_address;
    unsigned short port = 8080;

    proto = getprotobyname("tcp");
    if (proto == NULL)
    {
        perror("getprotobyname");
        exit(EXIT_FAILURE);
    }

    server_fd = socket(AF_INET, SOCK_STREAM, proto->p_proto);
    if (server_fd == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &enable_reuseaddr, sizeof(enable_reuseaddr)) < 0)
    {
        perror("setsockopt(SO_REUSEADDR) failed");
        exit(EXIT_FAILURE);
    }

    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(port);

    if (bind(server_fd, (struct sockaddr *)&server_address, sizeof(server_address)) == -1)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 5) == -1)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "listening on port %d\n", port);

    while (1)
    {
        client_len = sizeof(client_address);
        printf("waiting for client\n");
        client_fd = accept(server_fd, (struct sockaddr *)&client_address, &client_len);
        filestream = open(file_name, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
        if (filestream == -1)
        {
            perror("open");
            exit(EXIT_FAILURE);
        }
        do
        {
            read_status = read(client_fd, buffer, BUFSIZ);
            if (read_status == -1)
            {
                perror("read");
                exit(EXIT_FAILURE);
            }
            if (write(filestream, buffer, read_status) == -1)
            {
                perror("write");
                exit(EXIT_FAILURE);
            }
        } while (read_status > 0);
        close(filestream);
        close(client_fd);
    }
    exit(EXIT_SUCCESS);
}
```

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <ctype.h>
#include <errno.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <wait.h>

void *zip_file()
{
    pid_t child_id;
    int status;

    if (child_id == fork())
    {
        char *argv[] = {"zip", "-r", "hartakarun.zip", "-q", "/home/ubuntu/shift3/hartakarun", NULL};
        execv("/bin/zip", argv);
    }

    while (wait(&status) > 0)
        ;
}

int main(int argc, char *argv[])
{
    char *file_name = "hartakarun.zip";
    char *hostname = "127.0.0.1";
    char buffer[BUFSIZ];
    in_addr_t in_address;
    int filestream;
    int sock;
    ssize_t read_status;
    struct hostent *host;
    struct protoent *proto;
    struct sockaddr_in socket_address;
    unsigned short port = 8080;

    char cmd[100];
    scanf("%[^\n]s", cmd);
    if (strcmp(cmd, "send hartakarun.zip") != 0)        
    {
        printf("Wrong command");
        return 0;
    }

    zip_file();

    if (argc > 1)
    {
        if (strcmp(argv[1], "send") == 0)
        {
            if (argc > 2)
            {
                file_name = argv[2];
            }
            else
                exit(EXIT_FAILURE);
        }
        else
            exit(EXIT_FAILURE);
    }

    filestream = open(file_name, O_RDONLY);
    if (filestream == -1)
    {
        perror("open");
        exit(EXIT_FAILURE);
    }

    proto = getprotobyname("tcp");
    if (proto == NULL)
    {
        perror("getprotobyname");
        exit(EXIT_FAILURE);
    }

    sock = socket(AF_INET, SOCK_STREAM, proto->p_proto);
    if (sock == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    host = gethostbyname(hostname);
    if (host == NULL)
    {
        fprintf(stderr, "error: gethostbyname(\"%s\")\n", hostname);
        exit(EXIT_FAILURE);
    }

    in_address = inet_addr(inet_ntoa(*(struct in_addr *)*(host->h_addr_list)));
    if (in_address == (in_addr_t)-1)
    {
        fprintf(stderr, "error: inet_addr(\"%s\")\n", *(host->h_addr_list));
        exit(EXIT_FAILURE);
    }

    socket_address.sin_addr.s_addr = in_address;
    socket_address.sin_family = AF_INET;
    socket_address.sin_port = htons(port);

    if (connect(sock, (struct sockaddr *)&socket_address, sizeof(socket_address)) == -1)
    {
        perror("connect");
        exit(EXIT_FAILURE);
    }

    while (1)
    {
        read_status = read(filestream, buffer, BUFSIZ);
        if (read_status == 0)
            break;
        if (read_status == -1)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
        if (write(sock, buffer, read_status) == -1)
        {
            perror("write");
            exit(EXIT_FAILURE);
        }
    }
    close(filestream);
    exit(EXIT_SUCCESS);
}

```

Pada source code diatas terdapat beberapa fungsi, pada client.c terdapat fungsi zip_file dimana fungsi ini sendiri berfungsi untuk mengzip 

folder-folder yang sudah dikategorikan agar nantinya ada file hartakarun.zip yang akan dikirim.
       
Selanjutnya pada int main terdapat filename yang nantinya akan diisi hartakarun.zip dan hostname dimana akan diisi oleh ip address dan juga port disini kita menggunakan 8080.

Selanjutnya ada cmd dimana if jika kita (user) menginput command send hartakarun.zip maka akan dilanjut ke fungsi selanjutnya, namun apabila kita salah dalam input command maka program akan muncul Wrong command kemudian keluar dan harus di run lagi.

Selanjutnya pada command if (argc > 1) digunakan untuk memastikan apakah file yang akan dikirim adalah hartakarun.zip.

Selanjutnya pada command filestream = open(file_name, O_RDONLY); digunakan untuk menampung file yang akan dikirim nantinya dalam mode read only/

Selanjutnya pada command proto = getprotobyname("tcp"); disini kita menggunakan socket tcp.

Selanjutnya pada command sock = socket(AF_INET, SOCK_STREAM, proto->p_proto); disini untuk mendapatkan socket.

Selanjutnya pada command host = gethostbyname(hostname); disini untuk mendapatkan hostname.

Selanjutnya pada command in_address = inet_addr(inet_ntoa(*(struct in_addr*)*(host->h_addr_list))); disini digunakan untuk mendapatkan addressnya kemudian atribut atribut sebelumnya di assign ke socket_address

Selanjutnya pada command if (connect(sock, (struct sockaddr*)&socket_address, sizeof(socket_address)) == -1) disini untuk menyambungkan client ke server.

Selanjutnya dilakukan perulangan untuk mendapatkan status dari pembacaan file untuk mengirimkan file sekaligus mememeriksa error pada saat read and write.

Beralih ke server.c, pada filename masih sama dengan client.c dan port juga masih sama, lalu disini untuk getprotobyname dan socket
masih sama, bedanya ada pada penamaan socketnya.

Selanjutnya pada command if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &enable_reuseaddr, sizeof(enable_reuseaddr)) < 0) disini untuk menetapkan opsi socket untuk melakukan bind.

Kemudian atribut atribut sebelumnya di assign ke server_address, kemudian kita melakukan bind dan listen untuk server.
Selanjutnya terdapat fprintf(stderr,"listening on port", port) untuk memastikan apakah server sudah connect dengan port yang diinginkan.

Kemudian dilakukan perulangan yang mendapatkan size dari client address, dan selanjutnya kita menunggu client untuk mengirimkan pesan.

Sesudah client mengirimkan pesan maka akan direspon oleh server dan pesannya diterima untuk di jalankan dengan syntax filestream = open(file_name O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);.

Selanjutnya dilakukan perulangan untuk mendapatkan status dari pembacaan file untuk menerima file yang dikirimkan sekaligus mememeriksa error pada saat read and write.

## Dokumentasi
<img src="soal3/1.png">
<img src="soal3/2.png">
<img src="soal3/3.png">
<img src="soal3/4.png">
---

note = untuk ss ke 4 menggunakan laptop berbeda karena ubuntu di laptop saya rusak (Antonio)
